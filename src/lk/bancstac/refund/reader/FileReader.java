/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.bancstac.refund.reader;

import com.sun.rowset.internal.Row;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import lk.bancstac.refund.request.RefundRequest;

/**
 *
 * @author chanaka
 */
public class FileReader {

    private String inputFile;
    RefundRequest refreq = new RefundRequest();
    private Node head;
    LinkedList<String> txnref = new LinkedList<>();
    LinkedList<String> amount = new LinkedList<>();
    LinkedList<String> clientref = new LinkedList<>();
    LinkedList<String> clientid = new LinkedList<>();
    
    class Node 
    { 
        int data; 
        Node next; 
        Node(int d) {data = d; next = null; } 
    } 

    void removeDuplicates() {
        /*Another reference to head*/
        Node curr = head;

        /* Traverse list till the last node */
        while (curr != null) {
            Node temp = curr;
            /*Compare current node with the next node and  
            keep on deleting them until it matches the current  
            node data */
            while (temp != null && temp.data == curr.data) {
                temp = temp.next;
            }
            /*Set current node next to the next different  
            element denoted by temp*/
            curr.next = temp;
            curr = curr.next;
        }
    }

    String[][] sheet_data = null;

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public void read() throws IOException {
        File inputWorkbook = new File(inputFile);
        Workbook w;

        try {
            w = Workbook.getWorkbook(inputWorkbook);

            Sheet sheet = w.getSheet(0);
            sheet_data = new String[sheet.getColumns()][sheet.getRows()];

            for (int j = 0; j < sheet.getRows(); j++) {
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    sheet_data[i][j] = cell.getContents();
                    System.out.println("");
                }
            }
            
       
            
//            for (int i = 0; i < sheet_data.length; i++) {
                for (int j = 0; j < sheet_data[6].length; j++) {
                    txnref.add(sheet_data[0][j]);
                    amount.add(sheet_data[2][j]);
                    clientref.add(sheet_data[3][j]);
                    clientid.add(sheet_data[6][j]);
                }
//            }
            
//            txnref.removeFirst();
//            amount.removeFirst();
//            clientref.removeFirst();
//            clientid.removeFirst();
            System.out.println("txnref :" + txnref);
            System.out.println("amount :" + amount);
            System.out.println("clientref :" + clientref);
            System.out.println("clientid :" + clientid);
            
            for(int i=1;i<txnref.size();i++){
                refreq.processrefund(txnref.get(i),amount.get(i),clientref.get(i),clientid.get(i));
            }
            
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

}
