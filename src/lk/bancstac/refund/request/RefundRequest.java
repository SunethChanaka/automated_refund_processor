/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.bancstac.refund.request;

import au.com.gateway.client.GatewayClient;
import au.com.gateway.client.component.TransactionAmount;
import au.com.gateway.client.config.ClientConfig;
import au.com.gateway.client.enums.TransactionType;
import au.com.gateway.client.ex.GatewayClientException;
import au.com.gateway.client.payment.PaymentRealTimeRequest;
import au.com.gateway.client.payment.PaymentRealTimeResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chanaka
 */
public class RefundRequest {

    public GatewayClient gateway_config(){
         ClientConfig config = new ClientConfig();
//Replace details provided, before using.
        config.setServiceEndpoint("https://sampath.paycorp.lk/rest/service/proxy/");
        config.setAuthToken("aaebe655-7aba-4ab5-b8f1-573950e02d57");
        config.setHmacSecret("xaEqU71uQcCUOr7S");
        GatewayClient client = new GatewayClient(config);
        return client;
    }
    
    public void processrefund(String txn_ref,String amount,String clientref,String clientId){
        try{
            float actual_amount = Float.parseFloat(amount);
            int client_Id = Integer.valueOf(clientId);
            System.out.println("test 1 :"+txn_ref.trim());
            System.out.println("test 2 :"+actual_amount);
            System.out.println("test 3 :"+clientref.trim());
            System.out.println("test 4 :"+client_Id);
        PaymentRealTimeRequest realTimeRequest = new PaymentRealTimeRequest();
        realTimeRequest.setClientId(client_Id);
        realTimeRequest.setTransactionType(TransactionType.REFUND);
        realTimeRequest.setOriginalTxnReference(txn_ref.trim());
// $10.10 dollars

        TransactionAmount txn = new TransactionAmount();
        txn.setCurrency("LKR");
        txn.setPaymentAmount((int) (actual_amount*100));
        txn.setTotalAmount((int) (actual_amount*100));
        realTimeRequest.setTransactionAmount(txn);

        realTimeRequest.setClientRef(clientref.trim());
        realTimeRequest.setComment("Paycorp Automated Refund Runner");
        PaymentRealTimeResponse realTimeResponse = gateway_config().payment().realTime(realTimeRequest);
        
        if(!(realTimeResponse.getResponseCode().equalsIgnoreCase("00")) || !(realTimeResponse.getResponseCode().equalsIgnoreCase("00"))){
            System.out.println("Please refer below failed txn references\n");
            System.out.println("Txn Ref :"+realTimeResponse.getTxnReference());
            System.out.println("Response code :"+realTimeResponse.getResponseCode());
        }
        else{
            System.out.println("\nPlease refer below successfull txn references\n"); 
            System.out.println("success Txn_ref"+realTimeResponse.getTxnReference()); 
        }
    }
    catch(GatewayClientException ex){
         Logger.getLogger(RefundRequest.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    
}
